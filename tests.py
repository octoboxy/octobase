#!/usr/bin/env python3
''' Thin wrapper around the test runner built-in to the base module.

To run this inside a pristine container:

docker run -it --rm --name octobasetest -e PYTHONPYCACHEPREFIX=/tmp -v .:/usr/src/octobase -w /usr/src/octobase \
    python:3.11-bookworm python tests.py
'''

import base

if __name__ == '__main__':
  base.TestRunner(basetest=True).RunFromCommandLine()
