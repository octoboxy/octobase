# Change Log

A terse log of changes, most recent to older:

- 0.9.1 -- 2024-04-05
  - RightDown renders HTML
- 0.9.0 -- 2024-03-31
  - RightDown moved in, not yet ready

.

- 0.8.6 -- 2024-01-04
  - adding FileTypes enum
- 0.8.5 -- 2023-12-21
  - bigger changes to logging
- 0.8.4 -- 2023-12-12
  - small changes to logging
- 0.8.3 -- 2023-12-01
  - python support moved back to 3.9
- 0.8.2 -- 2023-11-18
  - bugfixes
- 0.8.1 -- 2023-10-01
  - unit tests all pass inside a pristine container
- 0.8.0 -- 2023-09-11
  - `Option` constants now integral to `Enum`
  - added `Thing`, `ThreadStack`, `When`, and `regexp`
  - minimum python version now 3.10
  - PyPI status moved from *pre-alpha* to *beta*

.

- 0.6.10 -- 2022-01-31
  - CachedQuery now has no default expiry
- 0.6.9 -- 2022-01-16
  - bugfixes
- 0.6.8 -- 2021-11-22
  - experimental class `EnumOption`
     - *update @ 0.8.0:* now `Option`; no longer experimental
- 0.6.7 -- 2021-11-22
  - bugfix
- 0.6.6 -- 2021-06-10
  - rightdown fixes
- 0.6.5 -- 2021-06-05
  - bugfixes
- 0.6.4 -- 2021-05-08
  - `utils.IsContained()`
- 0.6.3 -- 2021-04-23
  - `AutoRegister` metaclass
- 0.6.2 -- *(never pushed)*
  - `utils.DeferImport()`
- 0.6.1 -- 2021-01-03
  - documentation example now actually works
- 0.6 -- 2021-01-02
  - new PyPI module, who dis?

