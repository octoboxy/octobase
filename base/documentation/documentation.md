---
icon: hexagon
...

# "OctoBase"

![OctoBase Logo](octobase.png)

## The First Building Block For Any Python Project

---

## Links

- Package on [PyPi](https://pypi.org/project/octobase/)
- Source code on [BitBucket](https://bitbucket.org/octoboxy/octobase/)
- [Installing Base](#installing_base)
- [Using Base](#using_base)
- The [Status and Future](status.md) of Base

---

## Installing Base

> ### Python Version
> 
> Moderate back compatibility with Python 3.9 is maintained.  We do our own development on Python 3.11 though, and may not notice bugs that exist on older systems.

Assuming your python environment is set up, install with `pip`:

```
$ sudo pip install octobase
```

The PyPi project is named "octobase" but the package you import is just `base`:

```
import base
print(base.VERSION)
```

Base has no required dependencies.  That is, a clean machine with appropriate python should be able to install and `import base`

### Optional Dependencies

The following libraries, if installed, extend our functionality:

- [`bleach`](https://pypi.org/project/bleach/) -- needed for `utils.StripHtml()`
- [`django`](https://pypi.org/project/Django/) -- enables several Django-specific utils
- [`numpy`](https://pypi.org/project/numpy/) -- needed for `utils.Entropy()`
- [`tzdata`](https://pypi.org/project/tzdata/) -- enables timezone parsing if the local system does not have `zoneinfo` libraries

---

## Using Base

> The OctoBase library is released under [Apache v2.0](http://www.apache.org/licenses/LICENSE-2.0) open-source license.

Base has *many* classes and functions.

The heart of the library is [RightDown](rightdown/rightdown.md), a full parser for markdown.  Check this out first, if you're looking for a place to start.

The soul of the library is `utils`, a very large collection of small code snips.  Sadly, `utils` is also by and large, undocumented.

> Obi Wan, Jedi Coder says, *"Use the source, Luke.  Let it guide your action."*

Excluding `utils`, every other high-level component of base *is* documented:

- [controllers](controllers.md)  --  third-prong of a "model-view-controller" paradigm
- [enums](enums.md)  --  better named constants
- [filetypes](filetypes.md)  --  file extensions and mime types
- [regexp](regexp.md) -- helpers for working with regular expressions
- [registry](registry.md) -- dictionary of names to objects
- [***rightdown***](rightdown/rightdown.md) -- ***a righteous markdown dialect***
- [testing](testing.md) -- our way of doing unit tests
- [things](things.md) -- new base class for everything
- [whens](whens.md) -- flexible (and extensible) time/date parsing

---

### Motivation

RightDown is *why* we bring you Base.

We believe markdown is a fantastic language for general data persistence, but that requires a solid markdown parser.  Thus, we built a markdown parser.  Then, we put it and all its dependencies into this library so we could give it away to everyone.

### Thank You!

Bug reports can be dropped in our [inbox](mailto:office@octoboxy.com).

If you can, please appreciate our efforts by donating to our [Kofi](https://ko-fi.com/fluidos).

Thank you ever so much!
