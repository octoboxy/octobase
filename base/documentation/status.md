---
icon: light-switch-on
...

# OctoBase

## Status and Future

---

## Status

The OctoBase library is released under [Apache v2.0](http://www.apache.org/licenses/LICENSE-2.0) open-source license.

As of the last update to this [documentation](documentation.md):

- Current version: 0.9.1
- Release date: 2024-04-05

Author notes:

> It's spring 2024.  After [more years](../../CHANGELOG.md) than we want to admit, the [RightDown](rightdown/rightdown.md) engine turns markdown into HTML.  Is it bug free?  Not a chance.  But it *is* ready for us all to rip out our old markdown engines and start using this one.
> 
> ### Help us out here
> 
> What weird esoteric markdown test case do you have that makes us break?

---

## Future

### Some Speculation

For the planned evolution of RightDown, see the top of the [RightDown](rightdown/rightdown.md) documentation.

The only non-RightDown task on our someday-maybe wishlist is:

- Whens
  - introduce BeforeCommonEra, with auto-conversions to/from CommonEra

But hey, what would be useful to you?

[Let us know!](mailto:office@octoboxy.com)
