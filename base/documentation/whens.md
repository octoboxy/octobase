# When (and CommonEra)

Want to know a generally unsolved ('till now) problem in Python?

Parsing dates and times.

Seriously.

#### why?

Python's `datetime` module includes method `strptime()` which *should* be able to recognize patterns of digits.  But in the fine print, if someone gives us a timezone, python completely punts on the problem of how to handle it.

Python didn't even standardize what a timezone *looks like* until Python 3.9, when they introduced the `zoneinfo` module.  This is a module that interfaces with linux-standard timezone database files, which are usually stored in the filesystem somewhere.  Notably, as this is linux tech, windows doesn't really work.  To solve this, the Python people made a PyPI module called `tzdata` that shims systems without zoneinfo databases.

But then, if you've followed me this deep into the rabbit hole, even all that gives you an incomplete mapping.

That is, all those 600-ish recognized timezones in the zoneinfo/tzdata database can tell you what the offset is from UTC to, say, "America/Denver" on some arbitrary day of the summer.  They can even tell you that normal people call that offset 'MDT' for short.

But still, if someone gives you a string that ends in the letters 'MDT', there is *nothing* in this entire stack of tech that will map that into an offset for you.

> Best solution I found is to in-memory reverse the zoneinfo database, sampling every zone at dates near each solstice.  See singleton class `base.utils.ZoneInfoByTzName` for details.  Let me know if you can think of anything more clever.

#### date, time, datetime

Additionally, you may run into the trouble that pythonic `date`, `time`, and `datetime` classes are very specific on type safety.  They are interchangeable in neither use nor storage.

#### dreams and fantasies

Let's add the ability to parse fantasy dates to our wishlist.  That is, our programmer is also a fantasy novelist, and wants to be able to parse date strings that don't necessarily use the same calendar we do.

### Solution

Our solution was to make our own class for parsing and holding time data.

---

## `base.When`

At its simplest, a `When` class parses all meaningful date and time data from a string.

```
base.When.From('2023-08-30')
```

`When.From()` returns a populated `When` on success, or None on failure.

### Example Strings

Each of these is a valid string lifted straight from our unit tests:

- `'2023-08-30'`
- `'2020-08-16 19:05:08.773308 MDT'`
- `'20th of August, 2023 at 13:28'`
- `'Feb 5, 2022 at 13:59 PST'`

Even filepaths or URLs:

- `'abcde:/Foo/2012-01-26/21.22.06.jpg'`

And of course, ISO 8601 and things that look almost like it:

- `'2023-08-20T11:19:27.209210-06:00'`

Beyond this, there is some limited support for dates relative to the current date:

- `'yesterday'`
- `'today'`
- `'tomorrow'`
- `'next friday'`
- `'last tuesday'`

As well as a few special dates and times together:

- `'now'`
- `'dawn of time'`
- `'end of days'`

### When Properties

A `When` object has all the standard attributes of both a date and a time: `.year`, `.month`, `.day`, `.weekday`, `.hour`, `.minute`, and `.second`.  This last might be a floating point number.  The rest are all integers.

There's also `.tzname` and `.timezone`, which may hold the short letter code, if it was parsed, and an actual zoneinfo instance, respectively.

There's also some high-level properties:

- `.text` -- convert the `When` back into a normalized string
- `.date` -- if possible, return a `datetime.date`
- `.time` -- if possible, return a `datetime.time`
- `.datetime` -- if possible, return a `datetime.datetime`

As well as:

- `.zero` -- True only if no meaningful date or time data is filled in
- `.cosmic` -- True if the `When` represents the dawn or dusk of eternity

### When Methods

The `When` also has two methods:

- `TimeZone()` -- populates `.timezone` from `.tzname`
- `Shift(timezone)` -- localizes our `When` to another timezone

### Cosmic Whens

If the When represents the dawn of time or end of days:

- `.cosmic` returns True
- `.date` returns `base.consts.DATE_MIN` or `_MAX`
- `.datetime` returns `base.consts.DATETIME_MIN` or `_MAX`

> Note that our _MIN/_MAX constants are very slightly offset from the datetime library's own mins and maxes, to allow our mins and maxes to be localized to various timezones without overflowing the underlying date or datetime types.

---

## `base.CommonEra`

The mechanical engine of this parsing all lives inside a class called `CommonEra`, which is a specialization of `Era`.  Every `When` is associated with an `Era`.  In the octobase library, `CommonEra` is the only `Era`.  But you might imagine other Eras in your own program that extend our framework onto other calendars, even fantasy calendars.

The magic of how parsing happens is through copious use of regular expressions, via our own [regexp](regexp.md) helper classes.
