---
icon: level-down-alt
...

# RightDown

## a righteous markdown dialect

### with a full markdown parser

---

> ## ==completeness==
>
> ### this project is in-progress
> 
> |     task             |     status     |
> |:---------------------|:---------------|
> | markdown parsing     | ***done***     |
> | metadata extraction  | ***done***     |
> | render standard html | ***done***     |
> | render naked text    | ***done***     |
> | render markdown      | *incomplete*   |
> | render better html   | *incomplete*   |
> | render pdf           | *not started*  |
> | documentation        | *mostly done*  | 
>
> our progress might be hastened by sending us some [kofi](https://ko-fi.com/fluidos)

---

## how to install

rightdown is part of the open-source python library [octobase](../documentation.md), which is installed via `pip`:

```
sudo pip install octobase
```

---

## how to use

### at a glance

given `thing` which is either a string or an open text file:

```
import base
rd = base.RightDown.From(thing)
```

or given a path to a file:

```
rd = base.RightDown.Load(filepath)
```

in either case the `rd` that's returned is an instance of `base.RightDown` and represents a fully parsed document

this `RightDown` object has properties and methods for many nifty things, like extracting metadata, and rendering to HTML or text:

```
print('Metadata:')
print(rd.metadata)  # dictionary

print('HTML:')
print(rd.Html())    # string

print('Normalized markdown:')
print(rd.Text())    # string
```

### tabs and spaces

parser options may be passed as keywords to `Rightdown.From()`

- `tab_width` -- how many spaces equals a tab -- *default: 2* 
- `indented_code_width` -- how many spaces for an indented code block -- *default: 4*

this means that any line indented four or more spaces is considered an indented code block, but a tab is only two spaces.  in other words, a single tab at the start of a line will not trigger such a block

if, for instance, you wanted tabs to trigger indented code blocks, then set `tab_width` to `4`:

```
rd = base.RightDown.From(thing, tab_width=4)
```

alternately, you could turn off indented code blocks completely:

```
rd = base.RightDown.From(thing, indented_code_width=0)
```

see the top of class `Parser` in `parser.py` for an authoritative list of all options

---

## rightdown syntax

all markdown libraries get to choose what language bits to have and what to leave out.  headers, block quotes, and links?  all pretty normal.  paragraph alignment?  not so common

### rightdown is just a dialect of markdown

rightdown is just a dialect of markdown, in that all standard markdown is supported.  we also include some non-standard extensions, such as paragraph alignment, blank lines, soft breaks, etc.

many details are explored in the following example documents:

- [paragraphs and whitespace](examples/paragraphs.md)
- [quotes, tables, and lists](examples/subblocks.md)
- [links, images, and icons](examples/links.md)
- [hard breaks, comments, and code blocks](examples/fragmenting.md)
- [inline formatting](examples/inline.md)
- [metadata extraction](examples/metadata.md)

---

## internals

a description of the library's internal implementation is available [here](engine.md)

---

## feedback

please write to us with any comments, questions, or criticisms:

- [office@octoboxy.com](mailto:office@octoboxy.com)
- [donations](https://ko-fi.com/fluidos)

thank you so much
