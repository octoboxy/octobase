# quotes, tables, and lists

---

## quotes

> ## quoted heading
>
> quoted body content

> next quoted block; note that in traditional markdown
> this would likely be seen as the same quoted block
>
> > quoted block inside quoted block
> >
> > > ### with a quoted heading inside that
> >
> > text after the third block


---

## tables

for fun, let's put our table in a quote

> |   Cat   |  Marks  |
> |:--------|--------:|
> | tiger   | stripes |
> | cheetah |   spots |

---

## lists

### much harder

simple interleaving:

1. one
  - one.one
2. two
  - two.one
  - two.two
3. three
  - three.one
  - three.two
  - three.three

a line of normal paragraph text

4. another whole list
  - interleaved with a list
    that has enough extra text that it's going to be wrapped
    to the next line, which should happen about here...
    - followed by another sub-list
  - this item
    has its own extra

D. yet another list
  A. with a sub-list

this is an indented code block, and not a list:

    - indented too far for a list

but this is a list:

- parent
    - child

these are all empty lists, and should be silently filtered out:

9.
-

10.
-

-

thanks for playing with me today!
