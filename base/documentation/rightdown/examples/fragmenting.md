---
description: esoteric cases of fragmenting
...

# hard breaks, comments, and code blocks

## easy mode

this fragment has a metadata block, two headings, and a paragraph.  simple

---

## fragment one

### fences and comments

this fragment includes a hard break inside a fenced block, and to be extra mean we even put empty lines around it.  here it comes:

```

---

```

this should be the exact same fragment as before

we're going to do the same trick now, but with comments around it all

/* stuff inside comment

```

---

```

more stuff inside comment */

one more time, swapping which is inside and which is outside

```

/* stuff inside comment

---

more stuff inside comment */

```

this should *still* be the same fragment.  the comment should eat the fenced code block, or the fenced block should eat the comment, but either should eat the fragment break.

// single-line comments are possible too

/* but for a single-line comment using this style to be recognized, there must *not* be any text on the line after the closing mark */

<!-- html comment style
works as well

even with embedded blank lines
though for predictable behavior, let's say it has similar restrictions on closing mark -->

<!-- single-line variation -->

---
color: purple
...

## fragment two

this fragment has its own metadata

from here on, we're just going to try to confuse the system with blank or nearly-blank fragments

---

---
---
<!-- comment -->
---

## fragment six

thanks for playing with me today!
