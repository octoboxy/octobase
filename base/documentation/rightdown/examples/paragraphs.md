# paragraphs and whitespace

word

plain paragraph

unicode ♥︎ data ♠︎ in ♦︎ paragraph ♣︎

you can force newlines into the middle of a paragraph by ending a line with a backslash.  let's try it here...\
and again...\
and done.

> in traditional markdown you would force a newline by ending a line with two spaces.  that works here too.  yet, most sane text editors strip trailing whitespace.  therefore, for the specific sake of maintaining this very test case that you are reading, we have introduced one tiny easter-egg into the rightdown engine.  each time the word SQUIGUELGMAS is seen at the end of a line of input data, it is replaced with two spaces.  let's try it here...SQUIGUELGMAS
and again...SQUIGUELGMAS
and done.

here *is* a hard break:

---

here is a soft break:

...

another soft break:

. . .

blank line:

.


these, however, do not trigger the above patterns because each one has more text after it on the same line:

--- not a hard break

... not a soft break

. not a blank

---  

> • this paragraph is quoted and is long enough, really it's long enough,
> i promise you for really reals it's long enough to need to have two lines

   • this paragraph is indented in the input text for some reason,
   and also has two lines, but nevertheless should come out as a
   normal paragraph

    • this text is not fenced, but is indented four spaces,
    
    which means it should be a singular code block

```
   • this text is fenced,
    
   and as such is a separate code block
```

---

> let's do paragraph alignment, and for fun, let's do it inside a blockquote
>
> <- fully justified text ->
>
> <- left-aligned text <-
>
> -> centered text <-
>
> -> right-aligned text ->

---

<div class='sketch'>
  for extremely pedantic page details, HTML can be directly injected
  into a rightdown file.  each HTML tag pattern is extracted separately.
  this means that here -- inside the body of a manually styled div -- it's
  like i'm inside any other paragraph.  said another way: *inline markdown
  formatting **is** allowed*
</div>

template-engine syntax should pass through without being affected by inline formatting.  for example, the quotes in this template tag are straight, not tilted:  {% tag with foo='bar' %}
