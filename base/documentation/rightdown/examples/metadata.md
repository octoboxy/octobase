---
icon: ruler-triangle
description:  tests metadata extraction
title2: overrides the value from the heading
field0
field1:
field2
: first child
: note that text in metadata *does not* get processed
field3:
  this thing is a long value, wrapped and continued
  across more than one line
...

# Mon Sep 4, 2023

### this title get overridden by the metadata block

#### fourth-level gets promoted to third level

##### fifth-level is not picked up

this first line of plain old paragraph text should be the excerpt

> `field0` is a field but `notafield` is not because `field0` is in the metadata block and `notafield` is not

notafield

field4:

field5: short value

field6
: note that in the extracted metadata, the content *is not* processed
: but the rendered field, of course, *is* processed

thanks for playing with me today!
