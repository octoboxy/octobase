# inline formatting

---

## don't panic

_any_ formatting character may be backslash-escaped, even...

- \`code ticks\`
- backslash -- \\

---

## paired marks

text surrounded with...

* `*` is *italicized*
* `**` is **bold**
* `***` is ***bold-italics***
* \` is `code`
* `=` is =highlighted=
* `_` is _underlined_
  * middle_of_word underlining is off
* `~` is ~strikethrough~
  * middle~of~word striking is on

traditional markdown requires two marks on some of these.
==we== __handle__ ~~this~~

---

## super- and sub-scripts

* superscript^(we do) with a `^`
* subscript~(we do) with a `~`

the parenthesis may be omitted on a single-word superscript:

- example^word
- example^1
- example^*

> *the corresponding subscript shortcut does not exist because being able to do middle-of-word strikethrough is more useful*

---

## elegant characters

- tilted quotes: 'single', "double"
- dashes: en--dash and em---dash
- ellipsis: ...
- arrows: -->, <--, <-->
- equivalences: +/-, ~=, !=, =/=
- fractions: 1/4, 1/2, 3/4, 1/3, 2/3, 1/5, 2/5, 3/5, 4/5, 1/6, 5/6, 1/8, 3/8, 5/8, 7/8
- counts: 0st, 0th, 1st, 2nd, 3rd, 4th, ... 9th, 10th, 11th, 12th, 13th, ... 21st, 22nd, 23rd, 24th, ...
- capitalist: (tm), (c), (r)

---

## esoterica

here follow some examples on how we match formatting marks:

#### fully valid markdown

> | markdown              | rendered            |
> |:----------------------|:--------------------|
> | `*yip *_yap_**`       | *yip *_yap_**       |
> | `*yip *yap *yarf***`  | *yip *yap *yarf***  |


#### somewhat questionable markdown

> | markdown              | rendered            |
> |:----------------------|:--------------------|
> | `*yip *yap`           | *yip *yap           |
> | `***yip yap*`         | ***yip yap*         |
> | `_yip *yap *yarf_***` | _yip *yap *yarf_*** |
> | `*****yip*****`       | *****yip*****       |
> | `====yap====`         | ====yap====         |
> | `===~~~yop===~~~`     | ===~~~yop===~~~     |
> | `*****`               | *****               |
> | `_yip yap`            | _yip yap            |
> | `yap_ yip`            | yap_ yip            |
> | `~wyv`                | ~wyv                |
