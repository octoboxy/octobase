# links, images, and icons

## basic markdown links

- type a full URL
  - example: https://example.com
- `[title](url)`
  - example: `[Root Page](/)` --> [Root Page](/)

## more sophisticated variants

the rightdown library allows for links to be built using other patterns, with the anticipation that a higher-level content management system will be rendering them into inter-page links.  to this end, we have added the following link styles:

- `[title](slug)`
  - example: `[Contact Page](contact)` --> [Contact Page](contact)
- `[[flags]](slug)`
  - example: `[[image size0]](root)` --> [[image size0]](root)
- `[(slug)]` -- which is a shortcut for `[[title noicon]](slug)`
  - example: `[(root)]` --> [(root)]

## inline formatting

inline formatting in link title text is allowed

- example: `[our *root* page](root)` --> [our *root* page](root)

## images

images should work as we expect:

- `![alt text](url)`
  - example: `![logo](/logo.png)` --> ![logo](/logo.png)

and our extended version:

- `![[flags]](slug)`
  - example: `![[gallery size1]]](placeholder)` --> ![[gallery size1]]](placeholder)

## icons

icons are not links but are similar syntax, and also a feature of higher-level content management systems

- `((icon))` or `((icon color))` or even `((color icon))`
  - example: `((green hexagon))` --> ((green hexagon))
