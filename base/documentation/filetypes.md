---
icon: file-alt
...

# `base.filetypes`

## File Extensions and Mime Types

This module holds one large [Enum](enums.md), that contains several nested enums, all to represent common types of files.

The FileTypes enum has an extra method over base Enum:

- `GetByExtension(ext)` -- finds and returns the filetype option that matches a file extension

Additionally, each filetype option has  properties and methods beyond typical options:

- `.extensions` -- tuple of file extensions
- `.contentype` -- the MIME content-type for this type of file
- `.BestExtension()` -- returns the first extension from `.extensions`
- `.LongestExtension()` -- returns the most descriptive extension from  `.extensions`
- `.Parents()` -- returns a list of filetypes that include the current option

Examples of usage:

```
>>> base.FileTypes.GetByExtension('.jpg') == base.filetypes.FILE_IMAGE_JPEG
True
>>> base.FileTypes.GetByExtension('.jpg') in base.filetypes.FILE_IMAGE_JPEG
True
>>> base.FileTypes.GetByExtension('.jpg') in base.filetypes.FILE_IMAGE
True
>>> base.filetypes.FILE_IMAGE_JPEG.LongestExtension()
'jpeg'
>>> base.filetypes.FILE_IMAGE_JPEG.contenttype
'image/jpeg'

```

> Note: the high-level `FileTypes` enum is imported directly into the top-level of `base`, but each specific option is defined within the sub-module `base.filetypes`
