# `base.Thing`

`Thing` is a very simple base class.

It does two things:

1. has a friendlier default `__repr__()`

2. `ATTRIBUTES=`, with helper functions

If you set an `ATTRIBUTES=` to a list of attribute names, then all these methods will do exactly what you expect:

- `Dupe()` -- return a duplicate of the current instance
- `Copy(other, nones=True)` -- copy attributes from the other instance
- `Reset()` -- set each of our own attributes to None

*Only* attributes listed in `ATTRIBUTES` are affected.

. . .

Some notes about `Copy()`:

First, every attribute listed in `ATTRIBUTES` is copied by simple assignment.  This makes our `Copy()` method more akin to a shallow copy than a deep copy.

Second, a keyword argument `nones` lets you specify if values that are None should be copied (default) or skipped.

Third, the *other* instance does not need to be the same type as the instance on which `Copy()` is called.  In fact, *other* need not even be a `Thing`.  If it is a `Thing`, then only attributes in the intersection of both instance's `ATTRIBUTES` will be copied.

